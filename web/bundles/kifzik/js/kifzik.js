var racine = '/kifzik/web/app_dev.php/OBI3LzC0WnpzX6xK1OxauDcNEq3fx6KmRYojW267cH4';
$(document).ready(function () {

    $('#downloadpp').click(function () {
        $('#userbundle_artiste_photoProfil_file').trigger('click');
    });

    $('#userbundle_artiste_photoProfil_file').change(function () {
        //console.log(this.value);
        var file = document.querySelector('#userbundle_artiste_photoProfil_file').files[0];
        var reader = new FileReader();
        reader.onloadend = function () {
            var imgPreview = document.getElementById('previewpp');
            imgPreview.src = reader.result;
            $('#previewpp').fadeIn(2000);
        };

        if (file) {
            reader.readAsDataURL(file); //reads the data as a URL            
        }
    });

    $('#downloadphoto-projet').click(function () {
        $('#mainbundle_projet_photo_file').trigger('click');
    });

    $('#mainbundle_projet_photo_file').change(function () {
        //console.log(this.value);
        var file = document.querySelector('#mainbundle_projet_photo_file').files[0];
        var reader = new FileReader();
        reader.onloadend = function () {
            var imgPreview = document.getElementById('previewphoto-projet');
            imgPreview.src = reader.result;
            $('#previewphoto-projet').fadeIn(2000);
        };

        if (file) {
            reader.readAsDataURL(file); //reads the data as a URL            
        }
    });

    $("#nl_ok_btn").click(function () {
        var email = $("#nl_input").val();
        if (email.trim() !== "") {
            if (isEmail(email.trim())) {
                addNewsletter(email);
            } else {
                $("#nl_alert").html('<div style="padding: 5px; margin-bottom: 5px; font-size: 0.9em" class="alert alert-danger alert-dismissible fade in" role="alert">' +
                        '<button style="right: 0px" type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">×</span>' +
                        '</button>' +
                        'Veuillez entrer un email valide' +
                        '</div>');
            }
        }
    });

    $(".editcp").click(function () {
        var cpid = (this.id.split('-'))[1];
        $('input[name=cplibelle-' + cpid + ']').removeAttr('disabled');
        $('textarea[name=cpdescription-' + cpid + ']').removeAttr('disabled');
    });
    
    $("#ptelephone").keydown(function() {
        controlSaisieMoovTelephone(this.value);
    });

    $("#ptelephone").keyup(function() {
        controlSaisieMoovTelephone(this.value);
    });

    $("#ptelephone").keypress(function() {
        controlSaisieMoovTelephone(this.value);
    });
    
    $("#ptelephonemtn").keydown(function() {
        controlSaisieMTNTelephone(this.value);
    });

    $("#ptelephonemtn").keyup(function() {
        controlSaisieMTNTelephone(this.value);
    });

    $("#ptelephonemtn").keypress(function() {
        controlSaisieMTNTelephone(this.value);
    });
});

function isEmail(email) {
    if (/^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/.test(email))
        return true;
    else
        return false;
}

function phoneValid(num) {
    if (/^0[1-68]([-. ]?[0-9]{2}){4}$/.test(num))
        return true;
    else
        return false;
}

function controlSaisieMoovTelephone(telephone) {
    if (validatePhoneMoovNo(telephone) === true) {
        $("#ptelephone-container").attr('class', 'form-group has-success');
        $("#pbtnok").attr('class', 'btn btn-success');
    } else {
        $("#pbtnok").attr('class', 'btn btn-success disabled');
        $("#ptelephone-container").attr('class', 'form-group has-error');
    }
}

function controlSaisieMTNTelephone(telephone) {
    if (validatePhoneMTNNo(telephone) === true) {
        $("#ptelephonemtn-container").attr('class', 'form-group has-success');
        $("#pbtnmtnok").attr('class', 'btn btn-default kifzik-ok-btn');
    } else {
        $("#pbtnmtnok").attr('class', 'btn btn-default kifzik-ok-btn disabled');
        $("#ptelephonemtn-container").attr('class', 'form-group has-error');
    }
}

function validatePhoneMoovNo(s) {
    if (s.length < 8 || s.length > 8) {
        return false;
    } else {
        for (var i = 0; i < s.length; i++) {
            var character = s.charAt(i);
            if (isDigit(character)) {
            } else {
                return false;
                break;
            }
        }
        var prefix = s.charAt(0)+''+s.charAt(1);
        var moovPrefix = ["95", "94", "64", "65", "63"];
        if(moovPrefix.indexOf(prefix) === -1){
            return false;
        }
        return true;
    }
}

function validatePhoneMTNNo(s) {
    if (s.length < 8 || s.length > 8) {
        return false;
    } else {
        for (var i = 0; i < s.length; i++) {
            var character = s.charAt(i);
            if (isDigit(character)) {
            } else {
                return false;
                break;
            }
        }
        var prefix = s.charAt(0)+''+s.charAt(1);
        var mtnPrefix = ["97", "96", "66", "67", "62", "61"];
        if(mtnPrefix.indexOf(prefix) === -1){
            return false;
        }
        return true;
    }
}

function isDigit(ch) {
    if (ch < '0' || ch > '9') {
        return false;
    } else {
        return true;
    }
}

function addNewsletter(email) {
    $.ajax({
        url: racine + '/newsletter/add',
        data: {email: email},
        type: 'GET',
        success: function (response) {
            if (response === "SUCCESS") {
                $("#nl_alert").html('<div style="padding: 5px; margin-bottom: 5px; font-size: 0.9em" class="alert alert-success alert-dismissible fade in" role="alert">' +
                        '<button style="right: 0px" type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">×</span>' +
                        '</button>' +
                        'Votre email a été enrégistré avec succès' +
                        '</div>');
                $("#nl_input").val('');
            } else {
                $("#nl_alert").html('<div style="padding: 5px; margin-bottom: 5px; font-size: 0.9em" class="alert alert-warning alert-dismissible fade in" role="alert">' +
                        '<button style="right: 0px" type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">×</span>' +
                        '</button>' +
                        'Votre email a été déjà enrégistré' +
                        '</div>');
            }
        },
        error: function () {
            $("#nl_alert").html('<div style="padding: 5px; margin-bottom: 5px; font-size: 0.9em" class="alert alert-danger alert-dismissible fade in" role="alert">' +
                    '<button style="right: 0px" type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    'Difficulté de connexion au serveur' +
                    '</div>');
        }
    });
}