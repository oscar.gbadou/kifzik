$(document).ready(function () {
    localStorage.setItem('other_projet_current_page', '1');

    $(window).scroll(function () {
//        console.log('win & scroll = ' + ($(window).scrollTop() + $(window).height()));
//        console.log('scroll = ' + $(window).scrollTop());
//        console.log('doc h -172 = ' + ($(document).height() - 172));
        if ($(window).scrollTop() + $(window).height() === $(document).height() - 172) {
            decouvrirAutreProjet(parseInt(localStorage.getItem('other_projet_current_page')) + 1);
        }
    });
});

function decouvrirAutreProjet(page) {
    $("#loader_decouvrir_autre_projet").fadeIn(500);
    $.ajax({
        url: '/web/app_dev.php/decouvrir/projets',
        data: {page: page},
        type: 'GET',
        success: function (response) {
            $("#loader_decouvrir_autre_projet").hide(500);
            localStorage.setItem('other_projet_current_page', parseInt(localStorage.getItem('other_projet_current_page')) + 1);
            $("#projet_container").html($("#projet_container").html() + response);
        },
        error: function () {
            console.log('error');
        }
    });
}