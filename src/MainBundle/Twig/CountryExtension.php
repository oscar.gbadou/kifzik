<?php

namespace MainBundle\Twig;
use Symfony\Component\Intl\Intl;

class CountryExtension extends \Twig_Extension {

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('country', array($this, 'countryFilter')),
        );
    }

    public function countryFilter($countryCode, $locale = "en") {
        $country = Intl::getRegionBundle()->getCountryName($countryCode);

        return $country;
    }

    public function getName() {
        return 'country_extension';
    }

}
