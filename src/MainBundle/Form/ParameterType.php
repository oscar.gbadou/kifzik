<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParameterType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('dureeVieProjet', 'integer', array(
                    'label' => 'Durée de vie d\'un projet',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('nbreProjetAccueil', 'integer', array(
                    'label' => 'Nombre de projet sur la page d\'accueil',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Parameter'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'mainbundle_parameter';
    }

}
