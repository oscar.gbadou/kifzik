<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('file', 'file', array(
                    'required' => false,
                    'label' => ' ',
                    'attr' => array(
                        'accept' => 'image/jpeg, image/png, image/gif'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Document'
        ));
    }
    
    /**
     * @return string
     */
    public function getName() {
        return 'mainbundle_document';
    }

}
