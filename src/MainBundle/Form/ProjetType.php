<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjetType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('categorie', 'entity', array(
                    'placeholder' => '* Choisir une catégorie',
                    'required' => true,
                    'label' => ' ',
                    'class' => "MainBundle:Categorie",
                    'choice_label' => "libelle",
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => '* Choisir une catégorie'
                    )
                ))
                ->add('titre', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Titre de votre projet',
                        'class' => 'form-control'
                    )
                ))
                ->add('description', 'textarea', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Description de votre projet',
                        'class' => 'form-control',
                        'rows' => 10
                    )
                ))
                ->add('lienYoutube', 'url', array(
                    'required' => false,
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => 'Lien Youtube de votre projet',
                        'class' => 'form-control'
                    )
                ))
                ->add('lienSoundcloud', 'url', array(
                    'required' => false,
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => 'Lien Sound cloud de votre projet',
                        'class' => 'form-control'
                    )
                ))
                ->add('objectifFinancier', 'integer', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Objectif financier en F CFA',
                        'class' => 'form-control'
                    )
                ))
                ->add('butDuFinancement', 'textarea', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* A quoi va servir ce financement',
                        'class' => 'form-control',
                        'rows' => 5
                    )
                ))
                ->add('photo', new DocumentType(), array(
                    'label' => ' ',
                    'attr' => array(
                        'style' => 'display: none'
                    )
                ))

        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Projet'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'mainbundle_projet';
    }

}
