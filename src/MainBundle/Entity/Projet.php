<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Projet
 *
 * @ORM\Table(name="projet")
 * @ORM\Entity(repositoryClass="MainBundle\Entity\ProjetRepository")
 */
class Projet {

    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Document", cascade={"persist"})
     */
    private $photo;

    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Categorie")
     */
    private $categorie;

    /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\ASoutenu", mappedBy="projet", cascade={"remove"})
     */
    private $aSoutenus;

    /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\ContrepartieProjet", mappedBy="projet", cascade={"remove"})
     */
    private $contrepartieProjets;

    /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\Soutenir", mappedBy="projet", cascade={"remove"})
     */
    private $soutiens;
    
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Artiste", inversedBy="projets")
     */
    private $artiste;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;
    
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="idYoutube", type="string", length=255, nullable=true)
     */
    private $idYoutube;

    /**
     * @var string
     *
     * @ORM\Column(name="idSoundCloud", type="string", length=255, nullable=true)
     */
    private $idSoundCloud;

    /**
     * @var string
     *
     * @ORM\Column(name="lienYoutube", type="string", length=255, nullable=true)
     */
    private $lienYoutube;

    /**
     * @var string
     *
     * @ORM\Column(name="lienSoundcloud", type="string", length=255, nullable=true)
     */
    private $lienSoundcloud;

    /**
     * @var integer
     *
     * @ORM\Column(name="objectifFinancier", type="integer")
     */
    private $objectifFinancier;

    /**
     * @var string
     *
     * @ORM\Column(name="butDuFinancement", type="text")
     */
    private $butDuFinancement;

    /**
     * @var bool
     *
     * @ORM\Column(name="valider", type="boolean")
     */
    private $valider;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    public function __construct() {
        $this->date = new \DateTime();
        $this->valider = false;
        $this->code = time() . '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Projet
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Projet
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set lienYoutube
     *
     * @param string $lienYoutube
     *
     * @return Projet
     */
    public function setLienYoutube($lienYoutube)
    {
        $this->lienYoutube = $lienYoutube;

        return $this;
    }

    /**
     * Get lienYoutube
     *
     * @return string
     */
    public function getLienYoutube()
    {
        return $this->lienYoutube;
    }

    /**
     * Set lienSoundcloud
     *
     * @param string $lienSoundcloud
     *
     * @return Projet
     */
    public function setLienSoundcloud($lienSoundcloud)
    {
        $this->lienSoundcloud = $lienSoundcloud;

        return $this;
    }

    /**
     * Get lienSoundcloud
     *
     * @return string
     */
    public function getLienSoundcloud()
    {
        return $this->lienSoundcloud;
    }

    /**
     * Set objectifFinancier
     *
     * @param integer $objectifFinancier
     *
     * @return Projet
     */
    public function setObjectifFinancier($objectifFinancier)
    {
        $this->objectifFinancier = $objectifFinancier;

        return $this;
    }

    /**
     * Get objectifFinancier
     *
     * @return integer
     */
    public function getObjectifFinancier()
    {
        return $this->objectifFinancier;
    }

    /**
     * Set butDuFinancement
     *
     * @param string $butDuFinancement
     *
     * @return Projet
     */
    public function setButDuFinancement($butDuFinancement)
    {
        $this->butDuFinancement = $butDuFinancement;

        return $this;
    }

    /**
     * Get butDuFinancement
     *
     * @return string
     */
    public function getButDuFinancement()
    {
        return $this->butDuFinancement;
    }

    /**
     * Set valider
     *
     * @param boolean $valider
     *
     * @return Projet
     */
    public function setValider($valider)
    {
        $this->valider = $valider;

        return $this;
    }

    /**
     * Get valider
     *
     * @return boolean
     */
    public function getValider()
    {
        return $this->valider;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Projet
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set photo
     *
     * @param \MainBundle\Entity\Document $photo
     *
     * @return Projet
     */
    public function setPhoto(\MainBundle\Entity\Document $photo = null)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return \MainBundle\Entity\Document
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set categorie
     *
     * @param \MainBundle\Entity\Categorie $categorie
     *
     * @return Projet
     */
    public function setCategorie(\MainBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \MainBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set artiste
     *
     * @param \UserBundle\Entity\Artiste $artiste
     *
     * @return Projet
     */
    public function setArtiste(\UserBundle\Entity\Artiste $artiste = null)
    {
        $this->artiste = $artiste;

        return $this;
    }

    /**
     * Get artiste
     *
     * @return \UserBundle\Entity\Artiste
     */
    public function getArtiste()
    {
        return $this->artiste;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Projet
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add aSoutenus
     *
     * @param \MainBundle\Entity\ASoutenu $aSoutenus
     * @return Projet
     */
    public function addASoutenus(\MainBundle\Entity\ASoutenu $aSoutenus)
    {
        $this->aSoutenus[] = $aSoutenus;

        return $this;
    }

    /**
     * Remove aSoutenus
     *
     * @param \MainBundle\Entity\ASoutenu $aSoutenus
     */
    public function removeASoutenus(\MainBundle\Entity\ASoutenu $aSoutenus)
    {
        $this->aSoutenus->removeElement($aSoutenus);
    }

    /**
     * Get aSoutenus
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getASoutenus()
    {
        return $this->aSoutenus;
    }

    /**
     * Add contrepartieProjets
     *
     * @param \MainBundle\Entity\ContrepartieProjet $contrepartieProjets
     * @return Projet
     */
    public function addContrepartieProjet(\MainBundle\Entity\ContrepartieProjet $contrepartieProjets)
    {
        $this->contrepartieProjets[] = $contrepartieProjets;

        return $this;
    }

    /**
     * Remove contrepartieProjets
     *
     * @param \MainBundle\Entity\ContrepartieProjet $contrepartieProjets
     */
    public function removeContrepartieProjet(\MainBundle\Entity\ContrepartieProjet $contrepartieProjets)
    {
        $this->contrepartieProjets->removeElement($contrepartieProjets);
    }

    /**
     * Get contrepartieProjets
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContrepartieProjets()
    {
        return $this->contrepartieProjets;
    }

    /**
     * Add soutiens
     *
     * @param \MainBundle\Entity\Soutenir $soutiens
     * @return Projet
     */
    public function addSoutien(\MainBundle\Entity\Soutenir $soutiens)
    {
        $this->soutiens[] = $soutiens;

        return $this;
    }

    /**
     * Remove soutiens
     *
     * @param \MainBundle\Entity\Soutenir $soutiens
     */
    public function removeSoutien(\MainBundle\Entity\Soutenir $soutiens)
    {
        $this->soutiens->removeElement($soutiens);
    }

    /**
     * Get soutiens
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSoutiens()
    {
        return $this->soutiens;
    }

    /**
     * Set idYoutube
     *
     * @param string $idYoutube
     * @return Projet
     */
    public function setIdYoutube($idYoutube)
    {
        $this->idYoutube = $idYoutube;

        return $this;
    }

    /**
     * Get idYoutube
     *
     * @return string 
     */
    public function getIdYoutube()
    {
        return $this->idYoutube;
    }

    /**
     * Set idSoundCloud
     *
     * @param string $idSoundCloud
     * @return Projet
     */
    public function setIdSoundCloud($idSoundCloud)
    {
        $this->idSoundCloud = $idSoundCloud;

        return $this;
    }

    /**
     * Get idSoundCloud
     *
     * @return string 
     */
    public function getIdSoundCloud()
    {
        return $this->idSoundCloud;
    }
}
