<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Soutenir
 *
 * @ORM\Table(name="soutenir")
 * @ORM\Entity(repositoryClass="MainBundle\Entity\SoutenirRepository")
 */
class Soutenir
{
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $fan;
    
    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Projet", inversedBy="soutiens")
     */
    private $projet;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    public function __construct() {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Soutenir
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set fan
     *
     * @param \UserBundle\Entity\User $fan
     * @return Soutenir
     */
    public function setFan(\UserBundle\Entity\User $fan = null)
    {
        $this->fan = $fan;

        return $this;
    }

    /**
     * Get fan
     *
     * @return \UserBundle\Entity\User 
     */
    public function getFan()
    {
        return $this->fan;
    }

    /**
     * Set projet
     *
     * @param \MainBundle\Entity\Projet $projet
     * @return Soutenir
     */
    public function setProjet(\MainBundle\Entity\Projet $projet = null)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \MainBundle\Entity\Projet 
     */
    public function getProjet()
    {
        return $this->projet;
    }
}
