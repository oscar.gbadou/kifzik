<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ASoutenu
 *
 * @ORM\Table(name="asoutenu")
 * @ORM\Entity(repositoryClass="MainBundle\Entity\ASoutenuRepository")
 */
class ASoutenu
{
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $fan;
    
    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Projet", inversedBy="aSoutenus")
     */
    private $projet;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     */
    private $telephone;
    
    /**
     * @var string
     *
     * @ORM\Column(name="transactionId", type="string", length=255, nullable=true)
     */
    private $transactionId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="authCode", type="string", length=255, nullable=true)
     */
    private $authCode;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="contrepartie", type="boolean", nullable=true)
     */
    private $contrepartie;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="anonyme", type="boolean", nullable=true)
     */
    private $anonyme;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="transactionReussie", type="boolean", nullable=true)
     */
    private $transactionReussie;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="transactionConfirmer", type="boolean", nullable=true)
     */
    private $transactionConfirmer;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="transactionRecu", type="boolean", nullable=true)
     */
    private $transactionRecu;

    /**
     * @var integer
     *
     * @ORM\Column(name="montant", type="integer")
     */
    private $montant;

    public function __construct() {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return ASoutenu
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set montant
     *
     * @param integer $montant
     * @return ASoutenu
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return integer 
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set projet
     *
     * @param \MainBundle\Entity\Projet $projet
     * @return ASoutenu
     */
    public function setProjet(\MainBundle\Entity\Projet $projet = null)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \MainBundle\Entity\Projet 
     */
    public function getProjet()
    {
        return $this->projet;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return ASoutenu
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fan
     *
     * @param \UserBundle\Entity\User $fan
     * @return ASoutenu
     */
    public function setFan(\UserBundle\Entity\User $fan = null)
    {
        $this->fan = $fan;

        return $this;
    }

    /**
     * Get fan
     *
     * @return \UserBundle\Entity\User 
     */
    public function getFan()
    {
        return $this->fan;
    }

    /**
     * Set transactionId
     *
     * @param string $transactionId
     * @return ASoutenu
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * Get transactionId
     *
     * @return string 
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set contrepartie
     *
     * @param boolean $contrepartie
     * @return ASoutenu
     */
    public function setContrepartie($contrepartie)
    {
        $this->contrepartie = $contrepartie;

        return $this;
    }

    /**
     * Get contrepartie
     *
     * @return boolean 
     */
    public function getContrepartie()
    {
        return $this->contrepartie;
    }

    /**
     * Set anonyme
     *
     * @param boolean $anonyme
     * @return ASoutenu
     */
    public function setAnonyme($anonyme)
    {
        $this->anonyme = $anonyme;

        return $this;
    }

    /**
     * Get anonyme
     *
     * @return boolean 
     */
    public function getAnonyme()
    {
        return $this->anonyme;
    }

    /**
     * Set transactionReussie
     *
     * @param boolean $transactionReussie
     * @return ASoutenu
     */
    public function setTransactionReussie($transactionReussie)
    {
        $this->transactionReussie = $transactionReussie;

        return $this;
    }

    /**
     * Get transactionReussie
     *
     * @return boolean 
     */
    public function getTransactionReussie()
    {
        return $this->transactionReussie;
    }

    /**
     * Set transactionConfirmer
     *
     * @param boolean $transactionConfirmer
     * @return ASoutenu
     */
    public function setTransactionConfirmer($transactionConfirmer)
    {
        $this->transactionConfirmer = $transactionConfirmer;

        return $this;
    }

    /**
     * Get transactionConfirmer
     *
     * @return boolean 
     */
    public function getTransactionConfirmer()
    {
        return $this->transactionConfirmer;
    }

    /**
     * Set transactionRecu
     *
     * @param boolean $transactionRecu
     * @return ASoutenu
     */
    public function setTransactionRecu($transactionRecu)
    {
        $this->transactionRecu = $transactionRecu;

        return $this;
    }

    /**
     * Get transactionRecu
     *
     * @return boolean 
     */
    public function getTransactionRecu()
    {
        return $this->transactionRecu;
    }

    /**
     * Set authCode
     *
     * @param string $authCode
     * @return ASoutenu
     */
    public function setAuthCode($authCode)
    {
        $this->authCode = $authCode;

        return $this;
    }

    /**
     * Get authCode
     *
     * @return string 
     */
    public function getAuthCode()
    {
        return $this->authCode;
    }
}
