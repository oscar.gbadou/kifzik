<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parameter
 *
 * @ORM\Table(name="parameter")
 * @ORM\Entity(repositoryClass="MainBundle\Entity\ParameterRepository")
 */
class Parameter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="dureeVieProjet", type="integer")
     */
    private $dureeVieProjet;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbreProjetAccueil", type="integer")
     */
    private $nbreProjetAccueil;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dureeVieProjet
     *
     * @param integer $dureeVieProjet
     * @return Parameter
     */
    public function setDureeVieProjet($dureeVieProjet)
    {
        $this->dureeVieProjet = $dureeVieProjet;

        return $this;
    }

    /**
     * Get dureeVieProjet
     *
     * @return integer 
     */
    public function getDureeVieProjet()
    {
        return $this->dureeVieProjet;
    }

    /**
     * Set nbreProjetAccueil
     *
     * @param integer $nbreProjetAccueil
     * @return Parameter
     */
    public function setNbreProjetAccueil($nbreProjetAccueil)
    {
        $this->nbreProjetAccueil = $nbreProjetAccueil;

        return $this;
    }

    /**
     * Get nbreProjetAccueil
     *
     * @return integer 
     */
    public function getNbreProjetAccueil()
    {
        return $this->nbreProjetAccueil;
    }
}
