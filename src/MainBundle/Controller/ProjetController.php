<?php

namespace MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use MainBundle\Entity\Projet;
use MainBundle\Entity\Soutenir;
use MainBundle\Entity\ASoutenu;
use MainBundle\Entity\ContrepartieProjet;
use MainBundle\Form\ProjetType;
use MainBundle\Gpay\Gpay;

class ProjetController extends Controller {

  public function payerAction(Request $request, $code, $type) {
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $telephone = $request->get('telephone');
    $montant = $request->get('montant');
    $anonyme = $request->get('anonyme');
    $contrepartie = $request->get('contrepartie');
    $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);

    $soutient = new ASoutenu();
    $soutient->setFan($currentUser)
    ->setMontant($montant)
    ->setProjet($projet)
    ->setTelephone($telephone)
    ->setAnonyme(($anonyme == 'oui') ? true : false)
    ->setContrepartie(($contrepartie == 'oui') ? true : false);
    $em->persist($soutient);
    $em->flush();
    //$this->get('session')->getFlashBag()->add('info', 'Votre transaction a réussi. Veuillez la confirmer');
    return $this->render('MainBundle:Projet:executerCodeUssd.html.twig', array(
      'projet' => $projet
    ));
  }

  public function confirmationPaiementAction(Request $request, $code) {
    $em = $this->getDoctrine()->getManager();
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);
    $uac = $request->get('uac');
    $authCode = $request->get('authcode');
    $soutient = $em->getRepository('MainBundle:ASoutenu')
    ->findOneBy(array(
      'projet' => $projet,
      'fan' => $currentUser,
      'authCode' => $authCode
    ));
    $gpay = new Gpay();
    if ($gpay->charge($uac, $authCode)) {
      $soutient->setTransactionId($gpay->getTransaction_id())
      ->setTransactionConfirmer(true)
      ->setTransactionReussie(true);
      $em->flush();
      $this->get('session')->getFlashBag()->add('success', 'Votre transaction a été bien effectuée. Merci d\'avoir soutenu ce projet');
      return $this->redirect($this->generateUrl('main_detail_projet', array(
        'code' => $code
      )));
    } else {
      $this->get('session')->getFlashBag()->add('error', $gpay->getErrorCode() . ' : ' . $gpay->getErrorDescription());
      return $this->redirect($this->generateUrl('main_soutenir_projet', array(
        'code' => $code
      )));
    }
  }

  public function deleteAction($code) {
    $em = $this->getDoctrine()->getManager();
    $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);
    $em->remove($projet);
    $em->flush();
    $this->get('session')->getFlashBag()->add('success', 'Votre projet a été supprimé avec succès');
    return $this->redirect($this->generateUrl('main_artiste_project'));
  }

  public function editAction($code) {
    $em = $this->getDoctrine()->getManager();
    $request = $this->get('request');
    $contrepartieFans = $em->getRepository('MainBundle:ContrepartieFan')->findAll();
    $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);
    if ($projet && !$projet->getValider()) {
      $form = $this->createForm(new ProjetType(), $projet);
      $form->handleRequest($request);

      if ($request->getMethod() == 'POST') {
        if ($form->isValid()) {
          $em->persist($projet);
          $em->flush();

          foreach ($projet->getContrepartieProjets() as $c) {
            $contrepartie = $em->getRepository('MainBundle:ContrepartieProjet')->find($c->getId());
            $contrepartie->setDescription(($request->get('cpdescription-' . $c->getId())) ? $request->get('cpdescription-' . $c->getId()) : $c->getDescription())
            ->setLibelle(($request->get('cplibelle-' . $c->getId())) ? $request->get('cplibelle-' . $c->getId()) : $c->getLibelle())
            ->setMontant($c->getMontant());
            $em->persist($contrepartie);
            $em->flush();
          }

          $this->get('session')->getFlashBag()->add('success', 'Votre projet a été modifié avec succès et est toujours en cours de validation');
          return $this->redirect($this->generateUrl('main_artiste_project'));
        }
      }

      $this->get('session')->getFlashBag()->add('info', 'Vous pouvez toujours modifier tant que ce n\'est pas encore validé');
      return $this->render('MainBundle:Projet:edit.html.twig', array(
        'form' => $form->createView(),
        'contrepartieFans' => $contrepartieFans,
        'projet' => $projet
      ));
    } else {
      $this->get('session')->getFlashBag()->add('warning', 'Votre projet a étét déjà validé. Une fois valider vous ne pouvez plus le modifier');
      return $this->redirect($this->generateUrl('main_artiste_project'));
    }
  }

  public function addAction() {
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    $request = Request::createFromGlobals();
    $em = $this->getDoctrine()->getManager();
    $contrepartieFans = $em->getRepository('MainBundle:ContrepartieFan')->findAll();
    $newProjet = new Projet();
    $form = $this->createForm(new ProjetType(), $newProjet);
    $form->handleRequest($request);

    if ($request->getMethod() == 'POST') {
      if ($form->isValid()) {
        $newProjet->setArtiste($currentUser);
        $em->persist($newProjet);
        $em->flush();

        foreach ($contrepartieFans as $c) {
          $newContrepartie = new ContrepartieProjet();
          $newContrepartie->setDescription(($request->get('cpdescription-' . $c->getId())) ? $request->get('cpdescription-' . $c->getId()) : $c->getDescription())
          ->setLibelle(($request->get('cplibelle-' . $c->getId())) ? $request->get('cplibelle-' . $c->getId()) : $c->getLibelle())
          ->setMontant($c->getMontant())
          ->setProjet($newProjet);
          $em->persist($newContrepartie);
          $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', 'Votre projet a été créé avec succès et est en cours de validation');
        return $this->redirect($this->generateUrl('main_artiste_project'));
      }
    }
    return $this->render('MainBundle:Projet:add.html.twig', array(
      'form' => $form->createView(),
      'contrepartieFans' => $contrepartieFans
    ));
  }

  public function detailAction($code) {
    $em = $this->getDoctrine()->getManager();
    $parameter = $em->getRepository('MainBundle:Parameter')->find(1);
    $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);
    if($projet){
      $contrepartieFans = $em->getRepository('MainBundle:ContrepartieFan')->findAll();
      return $this->render('MainBundle:Projet:detail.html.twig', array(
        'projet' => $projet,
        'contrepartieFans' => $contrepartieFans,
        'parameter' => $parameter
      ));
    }else{
      throw $this->createNotFoundException('La projet que vous cherchez n\'existe pas');
    }
  }

  public function soutenirAction($code) {
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);
    $contrepartieFans = $em->getRepository('MainBundle:ContrepartieFan')->findAll();
    $newSoutien = new Soutenir();
    $newSoutien->setFan($currentUser)
    ->setProjet($projet);
    $em->persist($newSoutien);
    $em->flush();
    return $this->render('MainBundle:Projet:soutenir.html.twig', array(
      'projet' => $projet,
      'contrepartieFans' => $contrepartieFans
    ));
  }

  public function aSoutenuAction(Request $request) {
    $em = $this->getDoctrine()->getManager();
    $response = new Response();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    $fan = $em->getRepository('UserBundle:User')->findOneByTelephone($request->get('telephone'));
    if ($fan) {
      $dernierProjetConsulter = $em->getRepository('MainBundle:Soutenir')
      ->createQueryBuilder('s')
      ->leftJoin('s.fan', 'f')
      ->where('f.id = :idFan')
      ->orderBy('s.date', 'DESC')
      ->setParameter('idFan', $fan->getId())
      ->setFirstResult(0)
      ->setMaxResults(1)
      ->getQuery()
      ->getResult();

      $aSoutenu = new ASoutenu();
      $aSoutenu->setFan($fan)
      ->setMontant($request->get('montant'))
      ->setProjet(($dernierProjetConsulter) ? $dernierProjetConsulter[0]->getProjet() : null);
      $em->persist($aSoutenu);
      $em->flush();
    } else {
      $aSoutenu = new ASoutenu();
      $aSoutenu->setMontant($request->get('montant'))
      ->setTelephone($request->get('telephone'));
      $em->persist($aSoutenu);
      $em->flush();
    }
    $response->setContent("SUCCESS");
    return $response;
  }

  public function decouvrirAction() {
    $em = $this->getDoctrine()->getManager();
    $parameter = $em->getRepository('MainBundle:Parameter')->find(1);
    $created = new \DateTime(date('Y-m-d H:i:s', strtotime('-' . $parameter->getDureeVieProjet() . ' day')));
    $projets = $em->getRepository('MainBundle:Projet')
    ->createQueryBuilder('p')
    ->where('p.date >= :created')
    ->andWhere('p.valider = :true')
    ->setParameter('created', $created)
    ->setParameter('true', true)
    ->orderBy('p.date', 'DESC')
    ->setFirstResult(0)
    ->setMaxResults(16)
    ->getQuery()
    ->getResult();
    return $this->render('MainBundle:Projet:decouvrir.html.twig', array(
      'projets' => $projets,
      'parameter' => $parameter
    ));
  }

  public function decouvrirAutreAction(Request $request) {
    $page = $request->get('page');
    $em = $this->getDoctrine()->getManager();
    $parameter = $em->getRepository('MainBundle:Parameter')->find(1);
    $created = new \DateTime(date('Y-m-d H:i:s', strtotime('-' . $parameter->getDureeVieProjet() . ' day')));
    $premierProjet = ($page - 1) * 16;
    $totalProjets = $em->getRepository("MainBundle:Projet")->nbreProjetVisible($created);
    $totalPages = ceil(intval($totalProjets) / 16);
    $projets = $em->getRepository('MainBundle:Projet')
    ->createQueryBuilder('p')
    ->where('p.date >= :created')
    ->setParameter('created', $created)
    ->orderBy('p.date', 'DESC')
    ->setFirstResult($premierProjet)
    ->setMaxResults(16)
    ->getQuery()
    ->getResult();

    return $this->container->get('templating')->renderResponse('MainBundle:Projet:decouvrirAutre.html.twig', array(
      'projets' => $projets,
      'parameter' => $parameter
    ));
  }

  public function searchAction(Request $request) {
    $em = $this->getDoctrine()->getManager();
    $parameter = $em->getRepository('MainBundle:Parameter')->find(1);
    $created = new \DateTime(date('Y-m-d H:i:s', strtotime('-' . $parameter->getDureeVieProjet() . ' day')));
    $query = $request->get('q');
    $projets = $em->getRepository('MainBundle:Projet')
    ->createQueryBuilder('p')
    ->leftJoin('p.artiste', 'a')
    ->leftJoin('p.categorie', 'c')
    ->where('p.date >= :created')
    ->andWhere('p.titre LIKE :query OR p.description LIKE :query OR p.titre LIKE :query OR a.nom LIKE :query OR a.prenom LIKE :query OR a.nomArtiste LIKE :query OR c.libelle LIKE :query')
    ->setParameter('created', $created)
    ->setParameter('query', '%' . $query . '%')
    ->orderBy('p.date', 'DESC')
    ->getQuery()
    ->getResult();
    return $this->render('MainBundle:Projet:searchResult.html.twig', array(
      'projets' => $projets,
      'parameter' => $parameter,
      'query' => $query
    ));
  }

  public function meskifsAction() {
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $parameter = $em->getRepository('MainBundle:Parameter')->find(1);
    $projets = $em->getRepository('MainBundle:ASoutenu')->findByFan($currentUser);
    return $this->render('MainBundle:Projet:meskifs.html.twig', array(
      'projets' => $projets,
      'parameter' => $parameter,
    ));
  }

  public function projetParCategorieAction($id) {
    $em = $this->getDoctrine()->getManager();
    $parameter = $em->getRepository('MainBundle:Parameter')->find(1);
    $created = new \DateTime(date('Y-m-d H:i:s', strtotime('-' . $parameter->getDureeVieProjet() . ' day')));
    $categorie = $em->getRepository('MainBundle:Categorie')->find($id);
    $projets = $em->getRepository('MainBundle:Projet')
    ->createQueryBuilder('p')
    ->leftJoin('p.categorie', 'c')
    ->where('p.date >= :created')
    ->andWhere('p.valider = :true')
    ->andWhere('c.id = :catId')
    ->setParameter('created', $created)
    ->setParameter('true', true)
    ->setParameter('catId', $categorie->getId())
    ->orderBy('p.date', 'DESC')
    ->setFirstResult(0)
    ->setMaxResults($parameter->getNbreProjetAccueil())
    ->getQuery()
    ->getResult();
    return $this->render('MainBundle:Projet:projetParCategorie.html.twig', array(
      'projets' => $projets,
      'parameter' => $parameter,
      'categorie' => $categorie
    ));
  }

}
