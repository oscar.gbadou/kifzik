<?php

namespace MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MainBundle\Entity\Newsletter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NewsletterController extends Controller {

  public function addAction(Request $request) {
    $em = $this->getDoctrine()->getManager();
    $response = new Response();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    $old = $em->getRepository('MainBundle:Newsletter')->findOneByEmail($request->get('email'));
    if (!$old) {
      $new = new Newsletter();
      $new->setEmail($request->get('email'));
      $em->persist($new);
      $em->flush();
      $response->setContent("SUCCESS");

      $messageAdmin = \Swift_Message::newInstance()->setContentType('text/html')
      ->setSubject('Newsletter')
      ->setFrom(array('info@kifzik.com' => 'KIFZIK'))
      ->setTo('contact@kifzik.com')
      ->setBody($this->renderView('AdminBundle:Index:emailNewsletter.html.twig', array(
        'email' => $request->get('email'),
      )));
      $this->get('mailer')->send($messageAdmin);
    } else {
      $response->setContent("ERROR");
    }
    return $response;
  }

  public function inscriptionBBAAction(Request $request) {
    $response = new Response();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));

    $response->setContent("SUCCESS");

    $email = \Swift_Message::newInstance()->setContentType('text/html')
    ->setSubject('Inscription au Benin Blog Awards')
    ->setFrom(array('info@kifzik.com' => 'BENIN BLOG AWARDS'))
    ->setTo('oscar.gbadou@gmail.com')
    ->setBody($this->renderView('AdminBundle:Index:inscription_bba.html.twig', array(
      'email' => $request->get('email'),
      'categorie' => $request->get('categorie'),
      'nom' => $request->get('nom'),
      'prenoms' => $request->get('prenoms'),
      'telephone' => $request->get('telephone'),
      'titre' => $request->get('titre'),
      'description' => $request->get('description')
    )));
    $result = $this->get('mailer')->send($email);
    //return $this->render('MainBundle:Index:landingPage.html.twig');
    return $response;
  }

  public function contactBBAAction(Request $request) {
    $em = $this->getDoctrine()->getManager();
    $response = new Response();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));

    $response->setContent("SUCCESS");

    $email = \Swift_Message::newInstance()->setContentType('text/html')
    ->setSubject('Contact Benin Blog Awards')
    ->setFrom(array('info@kifzik.com' => 'BENIN BLOG AWARDS'))
    ->setTo('info@beninblogawards.com')
    ->setBody($this->renderView('AdminBundle:Index:contact_bba.html.twig', array(
      'email' => $request->get('email'),
      'name' => $request->get('name'),
      'msg' => $request->get('msg')
    )));
    $this->get('mailer')->send($email);
    return $response;
  }

}
