<?php

namespace MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller {

  public function kifzikTeamAction(){
    return $this->render('MainBundle:Index:team.html.twig');
  }

  public function cguAction(){
    return $this->render('MainBundle:Index:cgu.html.twig');
  }

  public function aboutAction(){
    return $this->render('MainBundle:Index:about.html.twig');
  }

  public function contactAction(Request $request){
    $defaultData = array();
    $form = $this->createFormBuilder($defaultData)
    ->add('name', 'text', array(
      'required'=>true,
      'attr'=>array(
        'class'=>'form-control',
        'placeholder'=>'Nom & prenoms'
      )
    ))
    ->add('email', 'email', array(
      'required'=>true,
      'attr'=>array(
        'class'=>'form-control',
        'placeholder'=>'Email'
      )
    ))
    ->add('telephone', 'text', array(
      'required'=>true,
      'attr'=>array(
        'class'=>'form-control',
        'placeholder'=>'Telephone'
      )
    ))
    ->add('message', 'textarea', array(
      'required'=>true,
      'attr'=>array(
        'class'=>'form-control',
        'placeholder'=>'Votre message',
        'rows'=>6
      )
    ))
    ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      // data is an array with "name", "email", and "message" keys
      $data = $form->getData();
      $email = \Swift_Message::newInstance()->setContentType('text/html')
              ->setSubject('New Contact')
              ->setFrom(array('info@kifzik.com' => 'KIFZIK'))
              ->setTo('contact@kifzik.com')
              ->setBody($this->renderView('MainBundle:Index:contact_email.html.twig', array(
                  'data' => $data,
                  'date'=> new \DateTime()
      )));
      $this->get('mailer')->send($email);
      $this->get('session')->getFlashBag()->add('success', 'Votre message a été envoyé avec succes.');
      return $this->redirect($this->generateUrl('main_homepage'));
    }
    return $this->render('MainBundle:Index:contact.html.twig', array(
      'form'=> $form->createView()
    ));
  }

  public function landingPageAction(){
    return $this->render('MainBundle:Index:landingPage.html.twig');
  }

  public function indexAction() {
    $em = $this->getDoctrine()->getManager();
    $parameter = $em->getRepository('MainBundle:Parameter')->find(1);
    $created = new \DateTime(date('Y-m-d H:i:s', strtotime('-' . $parameter->getDureeVieProjet() . ' day')));
    $projets = $em->getRepository('MainBundle:Projet')
    ->createQueryBuilder('p')
    ->where('p.date >= :created')
    ->andWhere('p.valider = :true')
    ->setParameter('created', $created)
    ->setParameter('true', true)
    ->orderBy('p.date', 'DESC')
    ->setFirstResult(0)
    ->setMaxResults($parameter->getNbreProjetAccueil())
    ->getQuery()
    ->getResult();
    return $this->render('MainBundle:Index:index.html.twig', array(
      'projets' => $projets,
      'parameter' => $parameter
    ));
  }

  public function profilArtisteAction() {
    $em = $this->getDoctrine()->getManager();
    $parameter = $em->getRepository('MainBundle:Parameter')->find(1);
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    return $this->render('MainBundle:Index:profilArtiste.html.twig', array(
      'artiste' => $currentUser,
      'parameter' => $parameter
    ));
  }

  public function artisteProjectAction() {
    $em = $this->getDoctrine()->getManager();
    $parameter = $em->getRepository('MainBundle:Parameter')->find(1);
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    return $this->render('MainBundle:Index:artisteProject.html.twig', array(
      'artiste' => $currentUser,
      'parameter' => $parameter
    ));
  }

  public function profilFanAction(){
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    return $this->render('MainBundle:Index:profilFan.html.twig', array(
      'fan' => $currentUser
    ));
  }

}
