<?php

namespace MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use MainBundle\Entity\ASoutenu;

class PaiementController extends Controller {

  public function paiementAction($code) {
    $em = $this->getDoctrine()->getManager();
    $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);
    return $this->render('MainBundle:Projet:paiement.html.twig', array(
      'projet'=>$projet
    ));
  }

  public function confirmPaiementAction(Request $request, $code) {
    $currentUser = $this->get('security.token_storage')->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);

    \Stripe\Stripe::setApiKey($this->getParameter('stripe_api_key'));

    // Token is created using Stripe.js or Checkout!
    // Get the payment token submitted by the form:
    $token = $request->get('token');
    $currency = $request->get('currency');
    $amount = $request->get('montant');
    $anonyme = $request->get('anonyme');
    $contrepartie = $request->get('contrepartie');

    try {
      // Use Stripe's library to make requests...

      // Charge the user's card:
      $charge = \Stripe\Charge::create(array(
        "amount" => ($currency != "xof")?$amount*100:$amount,
        "currency" => $currency,
        "description" => "Soutient à la campagne lancée par " . $projet->getArtiste()->getNomArtiste(),
        "source" => $token,
      ));

      if($currency == "xof"){
        $montant = $amount;
      }else if($currency == "eur"){
        $montant = $amount*655.98*100;
      }else{
        $montant = $amount*600*100;
      }
      $asoutenu = new ASoutenu();
      $asoutenu->setFan($currentUser)
      ->setProjet($projet)
      ->setMontant($montant)
      ->setAnonyme(($anonyme == 'oui') ? true : false)
      ->setContrepartie(($contrepartie == 'oui') ? true : false);
      $em->persist($asoutenu);
      $em->flush();
    } catch(\Exception $e) {
      $this->get('session')->getFlashBag()->add('error', 'Une erreur s\'est produit. Nous n\'avons pas pu effctuer le paiement. Message d\'erreur: '.$e->getMessage());
      return $this->redirect($this->generateUrl('main_projet_paiement', array(
        'code'=>$code
      )));
    }

    $this->get('session')->getFlashBag()->add('success', 'Paiement effectue avec succes');
    return $this->redirect($this->generateUrl('main_homepage'));
  }

}
