<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UserBundle\Mailer;

use FOS\UserBundle\Mailer\Mailer as BaseMailer;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\RouterInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Mailer\MailerInterface;

/**
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 */
class Mailer extends BaseMailer {

    protected $mailer;
    protected $router;
    protected $templating;
    protected $parameters;

    public function __construct($mailer, RouterInterface $router, EngineInterface $templating, array $parameters) {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->templating = $templating;
        $this->parameters = $parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function sendConfirmationEmailMessage(UserInterface $user) {

        $template = $this->parameters['confirmation.template'];
        if ($user instanceof Admin){
            $url = $this->router->generate('cielo_admin_registration_confirm', array('token' => $user->getConfirmationToken()), true);
        }else if($user instanceof AdminEntreprise){
            $url = $this->router->generate('cielo_admin_entreprise_registration_confirm', array('token' => $user->getConfirmationToken()), true);
        }else if($user instanceof Client){
            $url = $this->router->generate('cielo_client_registration_confirm', array('token' => $user->getConfirmationToken()), true);
        }else{
            $url = $this->router->generate('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), true);
        }
            
        $rendered = $this->templating->render($template, array(
            'user' => $user,
            'confirmationUrl' => $url
        ));
        $this->sendEmailMessage($rendered, $this->parameters['from_email']['confirmation'], $user->getEmail());
    }

    /**
     * {@inheritdoc}
     */
    public function sendResettingEmailMessage(UserInterface $user) {
        $template = $this->parameters['resetting.template'];
        $url = $this->router->generate('user_resetting_reset', array('token' => $user->getConfirmationToken()), true);
        $rendered = $this->templating->render('UserBundle:Resetting:email.txt.twig', array(
            'user' => $user,
            'confirmationUrl' => $url
        ));
        $this->sendEmailMessage($rendered, $this->parameters['from_email']['resetting'], $user->getEmail());
    }

}
