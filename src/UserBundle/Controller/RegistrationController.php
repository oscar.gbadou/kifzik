<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UserBundle\Entity\Artiste;
use UserBundle\Entity\Fan;
use UserBundle\Entity\User;
use UserBundle\Form\ArtisteType;
use UserBundle\Form\FanType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends Controller {

    public function artisteRegistrationAction() {
        $discriminator = $this->container->get('pugx_user.manager.user_discriminator');
        $discriminator->setClass('UserBundle\Entity\Artiste', true);
        $userManager = $this->container->get('pugx_user_manager');
        $newArtiste = new Artiste();
        $form = $this->createForm(new ArtisteType(), $newArtiste);
//        die(var_dump($form));
        $request = Request::createFromGlobals();
        $em = $this->getDoctrine()->getManager();
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
//            $form->bind($request);
            if ($form->isValid()) {
                $newArtisteM = $userManager->createUser();

                $newArtisteM->setNom($newArtiste->getNom());
                $newArtisteM->setPrenom($newArtiste->getPrenom());
                $newArtisteM->setEmail($newArtiste->getEmail());
                $newArtisteM->setPlainPassword($newArtiste->getPlainPassword());
                $newArtisteM->setNomArtiste($newArtiste->getNomArtiste());
                $newArtisteM->setPays($newArtiste->getPays());
                $newArtisteM->setTelephone($newArtiste->getTelephone());
                $newArtisteM->setDateNaissance($newArtiste->getDateNaissance());
                $newArtisteM->setSiteWeb($newArtiste->getSiteWeb());
                $newArtisteM->setLienPageFacebook($newArtiste->getLienPageFacebook());
                $newArtisteM->setLienPageTwitter($newArtiste->getLienPageTwitter());
                $newArtisteM->setLienPageYoutube($newArtiste->getLienPageYoutube());
                $newArtisteM->setLienPageSoundcloud($newArtiste->getLienPageSoundcloud());
                $newArtisteM->setBiographie($newArtiste->getBiographie());
                $newArtisteM->setPhotoProfil($newArtiste->getPhotoProfil());
//                $newArtisteM->setEnabled(true);
                $newArtisteM->addRole('ROLE_ARTISTE');


                $tokenGenerator = $this->container->get('fos_user.util.token_generator');
                $newArtisteM->setConfirmationToken($tokenGenerator->generateToken());

                $userManager->updateUser($newArtisteM, true);

                $message = \Swift_Message::newInstance()->setContentType('text/html')
                        ->setSubject('KIFZIK | Confirmation de compte')
                        ->setFrom('info@kifzik.com')
                        ->setTo($newArtisteM->getEmail())
                        ->setBody($this->renderView('UserBundle:Registration:email.html.twig', array(
                            'user' => $newArtisteM,
                            'confirmationUrl' => $_SERVER['HTTP_HOST'] . $this->generateUrl('user_registration_confirmation') . '?token=' . $newArtisteM->getConfirmationToken()
                )));
                $this->get('mailer')->send($message);

                $messageAdmin = \Swift_Message::newInstance()->setContentType('text/html')
                        ->setSubject('KIFZIK | Nouvelle inscription')
                        ->setFrom('info@kifzik.com')
                        ->setTo('contact@kifzik.com')
                        ->setBody($this->renderView('AdminBundle:Index:emailNewSubscription.html.twig', array(
                            'user' => $newArtisteM,
                )));
                $this->get('mailer')->send($messageAdmin);


                /* new connexion auto */
//                $user = $em->getRepository('UserBundle:User')->findOneByUsername($newArtiste->getEmail());
//                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
//                $this->get('security.token_storage')->setToken($token);


                /* old connexion auto  */
//
//                $token = new UsernamePasswordToken($userOne, null, 'main', $userOne->getRoles());
//                $this->get('security.context')->setToken($token);
//                $this->get('session')->set('_security_main', serialize($token));

                $this->get('session')->getFlashBag()->add('success', 'Votre compte a été créé avec succès. Veuillez consulter votre mail pour le confirmer');
                return $this->redirect($this->generateUrl('main_homepage'));
            }
        }
        return $this->render('UserBundle:Registration:artiste.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function fanRegistrationAction() {
        $discriminator = $this->container->get('pugx_user.manager.user_discriminator');
        $discriminator->setClass('UserBundle\Entity\Fan', true);
        $userManager = $this->container->get('pugx_user_manager');
        $newFan = new Fan();
        $form = $this->createForm(new FanType(), $newFan);
//        die(var_dump($form));
        $request = Request::createFromGlobals();
        $em = $this->getDoctrine()->getManager();
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
//            $form->bind($request);
            if ($form->isValid()) {
                $newArtisteM = $userManager->createUser();

                $newArtisteM->setNom($newFan->getNom());
                $newArtisteM->setPrenom($newFan->getPrenom());
                $newArtisteM->setEmail($newFan->getEmail());
                $newArtisteM->setPlainPassword($newFan->getPlainPassword());
                $newArtisteM->setPays($newFan->getPays());
                $newArtisteM->setTelephone($newFan->getTelephone());
//                $newArtisteM->setEnabled(true);
                $newArtisteM->addRole('ROLE_FAN');

                $tokenGenerator = $this->container->get('fos_user.util.token_generator');
                $newArtisteM->setConfirmationToken($tokenGenerator->generateToken());

                $userManager->updateUser($newArtisteM, true);

                $message = \Swift_Message::newInstance()->setContentType('text/html')
                        ->setSubject('KIFZIK | Confirmation de compte')
                        ->setFrom('info@kifzik.com')
                        ->setTo($newArtisteM->getEmail())
                        ->setBody($this->renderView('UserBundle:Registration:email.html.twig', array(
                            'user' => $newArtisteM,
                            'confirmationUrl' => $_SERVER['HTTP_HOST'] . $this->generateUrl('user_registration_confirmation') . '?token=' . $newArtisteM->getConfirmationToken()
                )));
                $this->get('mailer')->send($message);

                $messageAdmin = \Swift_Message::newInstance()->setContentType('text/html')
                        ->setSubject('KIFZIK | Nouvelle inscription')
                        ->setFrom('info@kifzik.com')
                        ->setTo('contact@kifzik.com')
                        ->setBody($this->renderView('AdminBundle:Index:emailNewSubscription.html.twig', array(
                            'user' => $newArtisteM,
                )));
                $this->get('mailer')->send($messageAdmin);

                $user = $em->getRepository('UserBundle:User')->findOneByUsername($newFan->getEmail());

//                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
//                $this->get('security.token_storage')->setToken($token);

                $this->get('session')->getFlashBag()->add('success', ' Votre compte a été créé avec succès. Veuillez consulter votre mail pour le confirmer');
                return $this->redirect($this->generateUrl('main_homepage'));
            }
        }
        return $this->render('UserBundle:Registration:fan.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function confirmationAction() {
        $request = $this->get('request');
        $token = $request->get('token');
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->findOneByConfirmationToken(trim($token));
        if ($user) {
            if (!$user->isEnabled()) {
                $user->setEnabled(true);
                $em->flush();

                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);

                $roles = $user->getRoles();
                if ($roles[0] == 'ROLE_FAN') {
                    $this->get('session')->getFlashBag()->add('success', ' Votre compte a été activé avec succès. Bienvenue sur votre compte KIFZIK');
                    return $this->redirect($this->generateUrl('main_profil_fan'));
                } else if ($roles[0] == 'ROLE_ARTISTE') {
                    $this->get('session')->getFlashBag()->add('success', ' Votre compte a été activé avec succès. Bienvenue sur votre compte KIFZIK');
                    return $this->redirect($this->generateUrl('main_artiste_profil'));
                }
            } else {
                $this->get('session')->getFlashBag()->add('warning', ' Compte déjà activé');
                return $this->redirect($this->generateUrl('main_artiste_profil'));
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', ' Ce compte n\'existe pas');
            return $this->redirect($this->generateUrl('main_homepage'));
        }
    }

    public function editFanProfilAction(Request $request) {
        $currentUser = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($currentUser->getId());
        $form = $this->createFormBuilder($currentUser)
                ->add('nom', 'text', array(
                    'label' => 'Nom',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('prenom', 'text', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('telephone', 'text', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('pays', 'country', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('email', 'email', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();
            $user->setNom($data->getNom());
            $user->setPrenom($data->getPrenom());
            $user->setEmail($data->getEmail());
            $user->setPays($data->getPays());
            $user->setTelephone($data->getTelephone());
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', ' Profil modifié avec succès');
            return $this->redirect($this->generateUrl('main_profil_fan'));
        }
        return $this->render('UserBundle:Profil:edit.html.twig', array(
                    'form' => $form->createView(),
        ));
    }
    
    public function editArtisteProfilAction(Request $request) {
        $currentUser = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($currentUser->getId());
        $form = $this->createFormBuilder($currentUser)
                ->add('nom', 'text', array(
                    'label' => 'Nom',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('prenom', 'text', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('telephone', 'text', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('pays', 'country', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('email', 'email', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('nomArtiste', 'text', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('dateNaissance', 'birthday', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('siteWeb', 'url', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('lienPageFacebook', 'url', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('lienPageTwitter', 'url', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('lienPageYoutube', 'url', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('lienPageSoundcloud', 'url', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();
            $user->setNom($data->getNom());
            $user->setPrenom($data->getPrenom());
            $user->setEmail($data->getEmail());
            $user->setPays($data->getPays());
            $user->setTelephone($data->getTelephone());
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', ' Profil modifié avec succès');
            return $this->redirect($this->generateUrl('main_artiste_profil'));
        }
        return $this->render('UserBundle:Profil:editArtiste.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}
