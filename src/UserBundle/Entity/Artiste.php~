<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;

/**
 * Artiste
 *
 * @ORM\Table(name="artiste")
 * @ORM\Entity(repositoryClass="UserBundle\Entity\ArtisteRepository")
 * @UniqueEntity(fields = "username", targetClass = "UserBundle\Entity\User", message="fos_user.username.already_used")
 * @UniqueEntity(fields = "email", targetClass = "UserBundle\Entity\User", message="fos_user.email.already_used")
 */
class Artiste extends User
{
    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Document", cascade={"persist"})
     */
    private $photoProfil;
    
    /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\Projet", mappedBy="artiste")
     */
    private $projets;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomArtiste", type="string", length=255)
     */
    private $nomArtiste;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateNaissance", type="datetime")
     */
    private $dateNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="siteWeb", type="string", length=255)
     */
    private $siteWeb;

    /**
     * @var string
     *
     * @ORM\Column(name="lienPageFacebook", type="string", length=255)
     */
    private $lienPageFacebook;

    /**
     * @var string
     *
     * @ORM\Column(name="lienPageTwitter", type="string", length=255)
     */
    private $lienPageTwitter;

    /**
     * @var string
     *
     * @ORM\Column(name="lienPageYoutube", type="string", length=255)
     */
    private $lienPageYoutube;

    /**
     * @var string
     *
     * @ORM\Column(name="lienPageSoundcloud", type="string", length=255)
     */
    private $lienPageSoundcloud;
    
    /**
     * @var string
     *
     * @ORM\Column(name="biographie", type="text")
     */
    private $biographie;


    /**
     * Set nomArtiste
     *
     * @param string $nomArtiste
     *
     * @return Artiste
     */
    public function setNomArtiste($nomArtiste)
    {
        $this->nomArtiste = $nomArtiste;

        return $this;
    }

    /**
     * Get nomArtiste
     *
     * @return string
     */
    public function getNomArtiste()
    {
        return $this->nomArtiste;
    }

    /**
     * Set dateNaissance
     *
     * @param \DateTime $dateNaissance
     *
     * @return Artiste
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    /**
     * Get dateNaissance
     *
     * @return \DateTime
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * Set siteWeb
     *
     * @param string $siteWeb
     *
     * @return Artiste
     */
    public function setSiteWeb($siteWeb)
    {
        $this->siteWeb = $siteWeb;

        return $this;
    }

    /**
     * Get siteWeb
     *
     * @return string
     */
    public function getSiteWeb()
    {
        return $this->siteWeb;
    }

    /**
     * Set lienPageFacebook
     *
     * @param string $lienPageFacebook
     *
     * @return Artiste
     */
    public function setLienPageFacebook($lienPageFacebook)
    {
        $this->lienPageFacebook = $lienPageFacebook;

        return $this;
    }

    /**
     * Get lienPageFacebook
     *
     * @return string
     */
    public function getLienPageFacebook()
    {
        return $this->lienPageFacebook;
    }

    /**
     * Set lienPageTwitter
     *
     * @param string $lienPageTwitter
     *
     * @return Artiste
     */
    public function setLienPageTwitter($lienPageTwitter)
    {
        $this->lienPageTwitter = $lienPageTwitter;

        return $this;
    }

    /**
     * Get lienPageTwitter
     *
     * @return string
     */
    public function getLienPageTwitter()
    {
        return $this->lienPageTwitter;
    }

    /**
     * Set lienPageYoutube
     *
     * @param string $lienPageYoutube
     *
     * @return Artiste
     */
    public function setLienPageYoutube($lienPageYoutube)
    {
        $this->lienPageYoutube = $lienPageYoutube;

        return $this;
    }

    /**
     * Get lienPageYoutube
     *
     * @return string
     */
    public function getLienPageYoutube()
    {
        return $this->lienPageYoutube;
    }

    /**
     * Set lienPageSoundcloud
     *
     * @param string $lienPageSoundcloud
     *
     * @return Artiste
     */
    public function setLienPageSoundcloud($lienPageSoundcloud)
    {
        $this->lienPageSoundcloud = $lienPageSoundcloud;

        return $this;
    }

    /**
     * Get lienPageSoundcloud
     *
     * @return string
     */
    public function getLienPageSoundcloud()
    {
        return $this->lienPageSoundcloud;
    }

    /**
     * Set biographie
     *
     * @param string $biographie
     *
     * @return Artiste
     */
    public function setBiographie($biographie)
    {
        $this->biographie = $biographie;

        return $this;
    }

    /**
     * Get biographie
     *
     * @return string
     */
    public function getBiographie()
    {
        return $this->biographie;
    }

    /**
     * Set photoProfil
     *
     * @param \MainBundle\Entity\Document $photoProfil
     *
     * @return Artiste
     */
    public function setPhotoProfil(\MainBundle\Entity\Document $photoProfil = null)
    {
        $this->photoProfil = $photoProfil;

        return $this;
    }

    /**
     * Get photoProfil
     *
     * @return \MainBundle\Entity\Document
     */
    public function getPhotoProfil()
    {
        return $this->photoProfil;
    }
}
