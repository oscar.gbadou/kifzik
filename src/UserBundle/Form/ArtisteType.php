<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArtisteType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Nom',
                        'class' => 'form-control'
                    )
                ))
                ->add('prenom', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Prénom',
                        'class' => 'form-control'
                    )
                ))
                ->add('email', 'email', array(
                    'label' => ' ',
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'placeholder' => '* Email',
                        'class' => 'form-control'
                    )
                ))
                ->remove('username', null, array(
                    'label' => ' ',
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'placeholder' => 'Nom d\'utilisateur',
                        'class' => 'form-control'
                    )
                ))
                ->add('plainPassword', 'repeated', array(
                    'type' => 'password',
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array(
                        'label' => ' ',
                        'attr' => array(
                            'placeholder' => '* Mot de passe',
                            'class' => 'form-control'
                        )
                    ),
                    'second_options' => array(
                        'label' => ' ',
                        'attr' => array(
                            'placeholder' => '* Resaisir mot de passe',
                            'class' => 'form-control'
                        )
                    ),
                    'invalid_message' => 'fos_user.password.mismatch',
                ))
                ->add('nomArtiste', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Nom d\'artiste',
                        'class' => 'form-control'
                    )
                ))
                ->add('pays', 'country', array(
                    'label' => ' ',
                    'placeholder' => '* Choisir votre pays',
                    'attr' => array(
                        'placeholder' => 'Pays',
                        'class' => 'form-control'
                    )
                ))
                ->add('telephone', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Téléphone, exple: 229XXXXXXXX',
                        'class' => 'form-control',
                        'pattern' => '^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$'
                    )
                ))
                ->add('dateNaissance', 'birthday', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Date de naissance',
                        'class' => 'form-control'
                    )
                ))
                ->add('siteWeb', 'url', array(
                    'required' => false,
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => 'Site web',
                        'class' => 'form-control'
                    )
                ))
                ->add('lienPageFacebook', 'url', array(
                    'required' => false,
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => 'Lien de votre page Facebook',
                        'class' => 'form-control'
                    )
                ))
                ->add('lienPageTwitter', 'url', array(
                    'required' => false,
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => 'Lien de votre compte Twitter',
                        'class' => 'form-control'
                    )
                ))
                ->add('lienPageYoutube', 'url', array(
                    'required' => false,
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => 'Lien de votre chaine Youtube',
                        'class' => 'form-control'
                    )
                ))
                ->add('lienPageSoundcloud', 'url', array(
                    'required' => false,
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => 'Lien de votre compte Sound Cloud',
                        'class' => 'form-control'
                    )
                ))
                ->add('biographie', 'textarea', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Biographie',
                        'class' => 'form-control',
                        'style' => 'margin-bottom: 10px'
                    )
                ))
                ->add('photoProfil', new \MainBundle\Form\DocumentType, array(
                    'label' => ' ',
                    'attr' => array(
                        'style' => 'display: none',
                        'class' => ''
                    )
                ))
        ;
    }

    public function getParent() {
        return 'fos_user_registration';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\Artiste'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'userbundle_artiste';
    }

}
