<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Nom',
                        'class' => 'form-control'
                    )
                ))
                ->add('prenom', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Prénom',
                        'class' => 'form-control'
                    )
                ))
                ->add('email', 'email', array(
                    'label' => ' ',
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'placeholder' => '* Email',
                        'class' => 'form-control'
                    )
                ))
                ->remove('username', null, array(
                    'label' => ' ',
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'placeholder' => '* Nom d\'utilisateur',
                        'class' => 'form-control'
                    )
                ))
                ->add('plainPassword', 'repeated', array(
                    'type' => 'password',
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array(
                        'label' => ' ',
                        'attr' => array(
                            'placeholder' => '* Mot de passe',
                            'class' => 'form-control'
                        )
                    ),
                    'second_options' => array(
                        'label' => ' ',
                        'attr' => array(
                            'placeholder' => '* Resaisir mot de passe',
                            'class' => 'form-control'
                        )
                    ),
                    'invalid_message' => 'fos_user.password.mismatch',
                ))
                ->add('pays', 'country', array(
                    'label' => ' ',
                    'placeholder' => '* Choisir votre pays',
                    'attr' => array(
                        'placeholder' => '* Pays',
                        'class' => 'form-control'
                    )
                ))
                ->add('telephone', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Téléphone, exple: 229XXXXXXXX',
                        'class' => 'form-control',
                        'pattern'=>'^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$'
                    )
                ))
        ;
    }
    

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\Admin'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'userbundle_admin';
    }
}
