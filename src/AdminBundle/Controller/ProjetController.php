<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MainBundle\Entity\Projet;
use MainBundle\Form\ProjetType;

class ProjetController extends Controller {

    public function allAction() {
        $em = $this->getDoctrine()->getManager();
        $projets = $em->getRepository('MainBundle:Projet')->findBy(array(), array('date' => 'DESC'));
        return $this->render('AdminBundle:Projet:all.html.twig', array(
                    'projets' => $projets
        ));
    }

    public function projetNonValideAction() {
        $em = $this->getDoctrine()->getManager();
        $projets = $em->getRepository('MainBundle:Projet')->findBy(array('valider' => false), array('date' => 'DESC'));
        return $this->render('AdminBundle:Projet:projetNonValide.html.twig', array(
                    'projets' => $projets
        ));
    }

    public function projetValideAction() {
        $em = $this->getDoctrine()->getManager();
        $projets = $em->getRepository('MainBundle:Projet')->findBy(array('valider' => true), array('date' => 'DESC'));
        return $this->render('AdminBundle:Projet:projetValide.html.twig', array(
                    'projets' => $projets
        ));
    }

    public function validerProjetAction($code) {
        $em = $this->getDoctrine()->getManager();
        $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);
        if ($projet && $projet->getValider()) {
            $projet->setValider(false);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Projet désactivé');
            return $this->redirect($this->generateUrl('admin_projet_valide'));
        } else {
            $projet->setValider(true);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Projet validé');
            return $this->redirect($this->generateUrl('admin_projet_non_valide'));
        }
    }

    public function editAction($code) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $contrepartieFans = $em->getRepository('MainBundle:ContrepartieFan')->findAll();
        $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);
//        $form = $this->createForm(new ProjetType(), $projet);
        $form = $this->createFormBuilder($projet)
                ->add('categorie', 'entity', array(
                    'placeholder' => '* Choisir une catégorie',
                    'required' => true,
                    'label' => ' ',
                    'class' => "MainBundle:Categorie",
                    'choice_label' => "libelle",
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => '* Choisir une catégorie'
                    )
                ))
                ->add('titre', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Titre de votre projet',
                        'class' => 'form-control'
                    )
                ))
                ->add('description', 'textarea', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Description de votre projet',
                        'class' => 'form-control',
                        'rows' => 10
                    )
                ))
                ->add('lienYoutube', 'url', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => 'Lien Youtube de votre projet',
                        'class' => 'form-control'
                    )
                ))
                ->add('lienSoundcloud', 'url', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => 'Lien Sound cloud de votre projet',
                        'class' => 'form-control'
                    )
                ))
                ->add('objectifFinancier', 'integer', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* Objectif financier en F CFA',
                        'class' => 'form-control'
                    )
                ))
                ->add('butDuFinancement', 'textarea', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => '* A quoi va servir ce financement',
                        'class' => 'form-control',
                        'rows' => 5
                    )
                ))
                ->add('idYoutube', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => 'Id Youtube',
                        'class' => 'form-control'
                    )
                ))
                ->add('idSoundCloud', 'text', array(
                    'label' => ' ',
                    'attr' => array(
                        'placeholder' => 'Id SoundCloud',
                        'class' => 'form-control'
                    )
                ))
                ->add('photo', new \MainBundle\Form\DocumentType(), array(
                    'label' => ' ',
                    'attr' => array(
                        'style' => 'display: none'
                    )
                ))
                ->getForm();
        $form->handleRequest($request);

        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $em->persist($projet);
                $em->flush();

                foreach ($projet->getContrepartieProjets() as $c) {
                    $contrepartie = $em->getRepository('MainBundle:ContrepartieProjet')->find($c->getId());
                    $contrepartie->setDescription(($request->get('cpdescription-' . $c->getId())) ? $request->get('cpdescription-' . $c->getId()) : $c->getDescription())
                            ->setLibelle(($request->get('cplibelle-' . $c->getId())) ? $request->get('cplibelle-' . $c->getId()) : $c->getLibelle())
                            ->setMontant($c->getMontant());
                    $em->persist($contrepartie);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'Projet modifié');
                if ($projet->getValider()) {
                    return $this->redirect($this->generateUrl('admin_projet_valide'));
                } else {
                    return $this->redirect($this->generateUrl('admin_projet_non_valide'));
                }
            }
        }

        return $this->render('AdminBundle:Projet:edit.html.twig', array(
                    'form' => $form->createView(),
                    'contrepartieFans' => $contrepartieFans,
                    'projet' => $projet
        ));
    }

    public function ficheProjetAction($code) {
        $em = $this->getDoctrine()->getManager();
        $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);
        $parameter = $em->getRepository('MainBundle:Parameter')->find(1);
        return $this->render('AdminBundle:Projet:ficheProjet.html.twig', array(
                    'projet' => $projet,
                    'parameter' => $parameter
        ));
    }

    public function soutientManifesteProjetAction($code) {
        $em = $this->getDoctrine()->getManager();
        $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);
        $soutients = $em->getRepository('MainBundle:Soutenir')->findByProjet($projet);
        return $this->render('AdminBundle:Projet:soutientManifeste.html.twig', array(
                    'soutients' => $soutients
        ));
    }

    public function soutientProjetAction($code) {
        $em = $this->getDoctrine()->getManager();
        $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);
        $soutients = $em->getRepository('MainBundle:ASoutenu')->findByProjet($projet);
        return $this->render('AdminBundle:Projet:soutient.html.twig', array(
                    'soutients' => $soutients
        ));
    }

    public function supprimerProjetAction($code) {
        $em = $this->getDoctrine()->getManager();
        $projet = $em->getRepository('MainBundle:Projet')->findOneByCode($code);
        $em->remove($projet);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Projet a été supprimé avec succès');
        return $this->redirect($this->generateUrl('admin_projet_all'));
    }

    public function projetArtisteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $artiste = $em->getRepository('UserBundle:Artiste')->find($id);
        $projets = $em->getRepository('MainBundle:Projet')->findByArtiste($artiste);
        return $this->render('AdminBundle:Projet:projetArtiste.html.twig', array(
                    'projets' => $projets
        ));
    }

    public function projetExpireAction() {
        $em = $this->getDoctrine()->getManager();
        $parameter = $em->getRepository('MainBundle:Parameter')->find(1);
        $created = new \DateTime(date('Y-m-d H:i:s', strtotime('-' . $parameter->getDureeVieProjet() . ' day')));
        $projets = $em->getRepository('MainBundle:Projet')
                ->createQueryBuilder('p')
                ->where('p.date < :created')
                ->setParameter('created', $created)
                ->orderBy('p.date', 'DESC')
                ->getQuery()
                ->getResult();
        return $this->render('AdminBundle:Projet:projetExpire.html.twig', array(
                    'projets' => $projets
        ));
    }

    public function projetAboutiAction() {
        $em = $this->getDoctrine()->getManager();
        $parameter = $em->getRepository('MainBundle:Parameter')->find(1);
        $projets = $em->getRepository('MainBundle:Projet')->findBy(array('valider' => true));
        $result = array();
        foreach ($projets as $p) {
            $soutiens = $p->getASoutenus();
            $montantRecolte = 0;
            foreach ($soutiens as $s) {
                $montantRecolte += $s->getMontant();
            }
            if ($montantRecolte == $p->getObjectifFinancier()) {
                $result[] = $p;
            }
        }
        return $this->render('AdminBundle:Projet:projetAbouti.html.twig', array(
                    'projets' => $result
        ));
    }

}
