<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MainBundle\Entity\ContrepartieFan;
use MainBundle\Form\ContrepartieFanType;
use Symfony\Component\HttpFoundation\Request;

class ContrepartieController extends Controller {

    public function listAction() {
        $em = $this->getDoctrine()->getManager();
        $contreparties = $em->getRepository('MainBundle:ContrepartieFan')->findAll();
        return $this->render('AdminBundle:Contrepartie:list.html.twig', array(
                    'contreparties' => $contreparties
        ));
    }

    public function addAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $newContrepartie = new ContrepartieFan();
        $form = $this->createForm(new ContrepartieFanType(), $newContrepartie);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $em->persist($newContrepartie);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', 'Contrepartie ajoutée avec succès');
                return $this->redirect($this->generateUrl('admin_contrepartie_list'));
            }
        }
        return $this->render('AdminBundle:Contrepartie:add.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $contrepartie = $em->getRepository('MainBundle:ContrepartieFan')->find($id);
        $form = $this->createForm(new ContrepartieFanType(), $contrepartie);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $em->persist($contrepartie);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', 'Contrepartie modifiée avec succès');
                return $this->redirect($this->generateUrl('admin_contrepartie_list'));
            }
        }
        return $this->render('AdminBundle:Contrepartie:add.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $contrepartie = $em->getRepository('MainBundle:ContrepartieFan')->find($id);
        $em->remove($contrepartie);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Contrepartie supprimée avec succès');
        return $this->redirect($this->generateUrl('admin_contrepartie_list'));
    }

}
