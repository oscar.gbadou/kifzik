<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller {

    public function indexAction() {
        return $this->render('AdminBundle:Index:index.html.twig');
    }

    public function sendEmailToContactAction() {
        $request = $this->get('request');
        $nom = $request->get('nom');
        $prenom = $request->get('prenom');
        $email = $request->get('email');
        $telephone = $request->get('telephone');
        $msg = $request->get('msg');

        $messageAdmin = \Swift_Message::newInstance()->setContentType('text/html')
                ->setSubject('KIFZIK | Contact')
                ->setFrom('info@kifzik.com')
                ->setTo('contact@kifzik.com')
                ->setBody($this->renderView('AdminBundle:Index:emailNewContact.html.twig', array(
                    'nom' => $nom,
                    'prenom' => $prenom,
                    'email' => $email,
                    'telephone' => $telephone,
                    'msg' => $msg,
        )));
        $this->get('mailer')->send($messageAdmin);
        $this->get('session')->getFlashBag()->add('success', 'Mail envoyé avec succès');
        return $this->redirect($this->generateUrl('main_homepage'));
    }

}
