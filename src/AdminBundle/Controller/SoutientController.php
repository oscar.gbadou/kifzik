<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SoutientController extends Controller {

    public function soutientManifesteAction() {
        $em = $this->getDoctrine()->getManager();
        $soutientManifeste = $em->getRepository('MainBundle:Soutenir')->findBy(array(), array('date' => 'DESC'));
        return $this->render('AdminBundle:Soutient:soutientManifeste.html.twig', array(
                    'soutientManifeste' => $soutientManifeste
        ));
    }
    
    public function soutientReelAction() {
        $em = $this->getDoctrine()->getManager();
        $soutientReel= $em->getRepository('MainBundle:ASoutenu')->findBy(array(), array('date' => 'DESC'));
        return $this->render('AdminBundle:Soutient:soutientReel.html.twig', array(
                    'soutientReel' => $soutientReel
        ));
    }

}
