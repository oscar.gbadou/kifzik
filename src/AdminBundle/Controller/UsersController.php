<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Admin;
use UserBundle\Form\AdminType;

class UsersController extends Controller {

    public function artistesAction() {
        $em = $this->getDoctrine()->getManager();
        $artistes = $em->getRepository('UserBundle:Artiste')->findBy(array(), array('id' => 'DESC'));
        return $this->render('AdminBundle:Users:artistes.html.twig', array(
                    'artistes' => $artistes
        ));
    }

    public function fansAction() {
        $em = $this->getDoctrine()->getManager();
        $fans = $em->getRepository('UserBundle:Fan')->findBy(array(), array('id' => 'DESC'));
        return $this->render('AdminBundle:Users:fans.html.twig', array(
                    'fans' => $fans
        ));
    }

    public function adminsAction() {
        $em = $this->getDoctrine()->getManager();
        $admins = $em->getRepository('UserBundle:Admin')->findBy(array(), array('id' => 'DESC'));
        return $this->render('AdminBundle:Users:admins.html.twig', array(
                    'admins' => $admins
        ));
    }

    public function profilFanAction($id) {
        $em = $this->getDoctrine()->getManager();
        $fan = $em->getRepository('UserBundle:Fan')->find($id);
        return $this->render('AdminBundle:Users:profilFan.html.twig', array(
                    'fan' => $fan
        ));
    }

    public function profilArtisteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $artiste = $em->getRepository('UserBundle:Artiste')->find($id);
        return $this->render('AdminBundle:Users:profilArtiste.html.twig', array(
                    'artiste' => $artiste
        ));
    }

    public function editProfilArtisteAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($id);
        $form = $this->createFormBuilder($user)
                ->add('nom', 'text', array(
                    'label' => 'Nom',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('prenom', 'text', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('telephone', 'text', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('pays', 'country', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('email', 'email', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('nomArtiste', 'text', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('dateNaissance', 'birthday', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('siteWeb', 'url', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('lienPageFacebook', 'url', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('lienPageTwitter', 'url', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('lienPageYoutube', 'url', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('lienPageSoundcloud', 'url', array(
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();
            $user->setNom($data->getNom());
            $user->setPrenom($data->getPrenom());
            $user->setEmail($data->getEmail());
            $user->setPays($data->getPays());
            $user->setTelephone($data->getTelephone());
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', ' Profil modifié avec succès');
            return $this->redirect($this->generateUrl('admin_profil_artiste', array(
                                'id' => $id
            )));
        }
        return $this->render('AdminBundle:Users:editProfilArtiste.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function desactiverCompteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($id);
        if ($user->isEnabled()) {
            $user->setEnabled(false);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', ' Compte désactivé avec succès');
        } else {
            $user->setEnabled(true);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', ' Compte activé avec succès');
        }
        $roles = $user->getRoles();
        if ($roles[0] == 'ROLE_ARTISTE') {
            return $this->redirect($this->generateUrl('admin_artistes'));
        } else if ($roles[0] == 'ROLE_FAN') {
            return $this->redirect($this->generateUrl('admin_fans'));
        } else {
            return $this->redirect($this->generateUrl('admin_admins'));
        }
    }

    public function supprimerCompteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($id);
        $roles = $user->getRoles();
        if ($roles[0] == 'ROLE_FAN') {
            $role = 'FAN';
        } elseif ($roles[0] == 'ROLE_ARTISTE') {
            $role = 'ARTISTE';
        } else {
            $role = 'ADMIN';
        }
        $em->remove($user);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', ' Compte supprimé avec succès');
        if ($role == 'ARTISTE') {
            return $this->redirect($this->generateUrl('admin_artistes'));
        } else if ($role == 'FAN') {
            return $this->redirect($this->generateUrl('admin_fans'));
        } else {
            return $this->redirect($this->generateUrl('admin_admins'));
        }
    }

    public function addAdminAction() {
        $discriminator = $this->container->get('pugx_user.manager.user_discriminator');
        $discriminator->setClass('UserBundle\Entity\Admin', true);
        $userManager = $this->container->get('pugx_user_manager');
        $newAdmin = new Admin();
        $form = $this->createForm(new AdminType(), $newAdmin);
//        die(var_dump($form));
        $request = Request::createFromGlobals();
        $em = $this->getDoctrine()->getManager();
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
//            $form->bind($request);
            if ($form->isValid()) {
                $newArtisteM = $userManager->createUser();

                $newArtisteM->setNom($newAdmin->getNom());
                $newArtisteM->setPrenom($newAdmin->getPrenom());
                $newArtisteM->setEmail($newAdmin->getEmail());
                $newArtisteM->setPlainPassword($newAdmin->getPlainPassword());
                $newArtisteM->setPays($newAdmin->getPays());
                $newArtisteM->setTelephone($newAdmin->getTelephone());
//                $newArtisteM->setEnabled(true);
                $newArtisteM->addRole('ROLE_ADMIN');

                $tokenGenerator = $this->container->get('fos_user.util.token_generator');
                $newArtisteM->setConfirmationToken($tokenGenerator->generateToken());

                $userManager->updateUser($newAdmin, true);

                $message = \Swift_Message::newInstance()->setContentType('text/html')
                        ->setSubject('KIFZIK | Confirmation de compte')
                        ->setFrom('info@kifzik.com')
                        ->setTo($newArtisteM->getEmail())
                        ->setBody($this->renderView('UserBundle:Registration:email.html.twig', array(
                            'user' => $newArtisteM,
                            'confirmationUrl' => $_SERVER['HTTP_HOST'] . $this->generateUrl('user_registration_confirmation') . '?token=' . $newArtisteM->getConfirmationToken()
                )));
                $this->get('mailer')->send($message);

                $messageAdmin = \Swift_Message::newInstance()->setContentType('text/html')
                        ->setSubject('KIFZIK | Nouvelle inscription')
                        ->setFrom('info@kifzik.com')
                        ->setTo('contact@kifzik.com')
                        ->setBody($this->renderView('AdminBundle:Index:emailNewSubscription.html.twig', array(
                            'user' => $newArtisteM,
                )));
                $this->get('mailer')->send($messageAdmin);

                $user = $em->getRepository('UserBundle:User')->findOneByUsername($newAdmin->getEmail());

//                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
//                $this->get('security.token_storage')->setToken($token);

                $this->get('session')->getFlashBag()->add('success', ' Votre compte a été créé avec succès. Veuillez consulter votre mail pour le confirmer');
                return $this->redirect($this->generateUrl('main_homepage'));
            }
        }
        return $this->render('UserBundle:Registration:admin.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}
