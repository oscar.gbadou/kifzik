<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MainBundle\Entity\Parameter;
use MainBundle\Form\ParameterType;

class ParameterController extends Controller {

    public function editAction() {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        $parameter = $em->getRepository('MainBundle:Parameter')->find(1);
        $form = $this->createForm(new ParameterType(), $parameter);
        $form->handleRequest($request);

        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $em->persist($parameter);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', 'Paramètre mise à jour');
                return $this->redirect($this->generateUrl('admin_parameter_edit'));
            }
        }
        return $this->render('AdminBundle:Parameter:edit.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}
